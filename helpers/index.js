export const formItems = [
    {
        title: 'Имя',
        id: 'first_name',
    },
    {
        title: 'Фамилия',
        id: 'second_name',
    },
    {
        title: 'Email',
        id: 'email',
    },
    {
        title: 'Пароль',
        id: 'password',
    },
    {
        title: 'Номер телефона',
        id: 'phone',
    },
    {
        title: 'Пол',
        id: 'gender',
    },
    {
        title: 'Адрес',
        id: 'address',
    },
    {
        title: 'Дата рождения',
        id: 'bdate',
    },
]